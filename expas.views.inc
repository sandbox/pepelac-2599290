<?php
// $Id$
/**
 * Implementation of hook_views_handlers()
 */
function expas_views_handlers() {
	$module_path = drupal_get_path('module', 'expas')."/handlers"; 
	return array(
     'handlers' => array(
      'expas_handler_field_count' => array(
        'parent' => 'views_handler_field_numeric',
        'path' => $module_path,
      ),
      'expas_handler_field_sum' => array(
       	'parent' => 'views_handler_field_numeric',
       	'path' => $module_path,	
      ),
      'expas_handler_filter_have_recense' => array(
      	'parent' => 'views_handler_filter_boolean_operator',
      	'path' => $module_path,
      ),      
     ),
   );
}
/**
 * Implementation of hook_views_data()
 */
function expas_views_data() {
  $data['expas_data_values']['table']['group'] = 'Expas module';
  $data['expas_data_values']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'id',
  	),
  );
  $data['expas_data_values']['nid'] = array(
    'title' => 'Количество рецензий',
    'help' => 'Количество рецензий экспертов в материале.',
    'field' => array(
      'handler' => 'expas_handler_field_count',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'label' => 'Наличие рецензий',
      'help' => 'Если активировано, будут показываться материалы с наличием рецензий.',
      'handler' => 'expas_handler_filter_have_recense',
      'type' => 'yes-no',
    ),
  );
  
  // recense_form_values
  $data['expas2']['table']['group'] = 'Expas module';
  $data['expas2']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'id',
  	),
  );
  $data['expas2']['nid'] = array(
    'title' => 'Общая оценка',
    'help' => 'Средняя арифм. оценка всех экспертов, оценивших материал.',
    'field' => array(
      'handler' => 'expas_handler_field_sum',
      'click sortable' => TRUE,
    ),
  );
  return $data;
}