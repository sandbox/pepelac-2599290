<?php
// $Id$
/**
 * handler for filter, show have recense for every node
 */
class expas_handler_filter_have_recense extends views_handler_filter_boolean_operator {
  function query() {
    $this->ensure_my_table();
    if ($GLOBALS['_GET']['nid'] == 0) {
    	$c = 1;
    	$sql = "(SELECT COUNT(nid) FROM {expas_data_values} edv WHERE edv.nid = node.nid && node.status = 1) = 0 ";
    }
    if ($GLOBALS['_GET']['nid'] == 1) {
    	$sql = "(SELECT COUNT(nid) FROM {expas_data_values} edv WHERE edv.nid = node.nid && node.status = 1) > 0 ";
    }
    elseif(empty($GLOBALS['_GET']['nid'])) {
    	$sql = "node.type IS NOT NULL";
    }
    $this->query->add_where($this->options['group'], $sql);
  }
}