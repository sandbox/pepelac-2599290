<?php
/**
 * get all count recenses from for node
 *
 */
class expas_handler_field_count extends views_handler_field_numeric {
  function query() {
    $table = $this->query->ensure_table('expas_data_values');
    $sql = "SELECT COUNT(uid) FROM {users} WHERE uid IN (SELECT uid FROM {expas_data_values} WHERE nid = node.nid)";
    $this->query->add_field('', "($sql)", 'count_recenses');
    $this->field_alias = 'count_recenses';
  }
  function render($values) {
    $txt = $values->count;
    if ($txt) {
      return $txt;
    }
    else {
      return parent::render($values);
    }
  }
}