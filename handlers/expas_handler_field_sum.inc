<?php
/**
 * get sum ball from recense
 *
 */
class expas_handler_field_sum extends views_handler_field_numeric {
  function query() {
    $table = $this->query->ensure_table('expas_data_values');
    if(function_exists('util_average_ball_between')) {
    	$add_sql = util_average_ball_between();
    	$add_sql = str_replace("v.eid","rfv2.eid", $add_sql);
    }
    //$sql = "SELECT FLOOR(SUM(rfv2.value)/count_recenses) FROM {expas_data_values} rfv2 WHERE rfv2.nid = node.nid && node.status = 1";
    $sql = "SELECT FLOOR(SUM(rfv2.value)/(SELECT COUNT(uid) FROM {users} WHERE uid IN (SELECT uid FROM {expas_data_values} WHERE nid = node.nid))) FROM {expas_data_values} rfv2 WHERE rfv2.nid = node.nid && node.status = 1 ".$add_sql;
    $this->query->add_field('', "($sql)", 'sum_recense_ball');
    $this->field_alias = 'sum_recense_ball';
  }

  function render($values) {
    $txt = $values->count;
    if ($txt) {
      return $txt;
    }
    else {
      return parent::render($values);
    }
  }
}