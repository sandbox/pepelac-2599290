// $Id$
if(Drupal.jsEnabled) {
	$(document).ready(function() {
		var server3 = $("#nodetype-234").attr('value');
		$(".expas_question").click(function(){
			$('.expas_question').attr('style','color:#999').removeClass('solid_border');
			$(this).attr('style','color:black').addClass('solid_border');
			var id = $(this).attr('value');
			$("#expas_data_question").hide().load(server3 + "/expas_quest/" + id + " #expas_show_quest", function() {
				$("#add-question-block").hide();
				$.getScript(server3 + "/sites/all/modules/expas/js/expas_value.js");
			}).fadeIn(400);
			return 0;
		});	
	});
}