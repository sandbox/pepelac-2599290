//обработчик после отсылки формы для автоматического раскрытия дерева
function expas_show_all(nodetype, block, question) {
	//блоки
	var server2 = $("#nodetype-234").attr('value');
	$('.expas_nodetype').attr('style','color:gray');
	$("#nodetype_" + nodetype).attr('style','color:black').addClass('solid_border');
	if(nodetype) {
		$("#expas_data_nodetype").hide().load(server2 + "/expas_block/"  + nodetype + " #expas_show_block", function(){ 
			$.getScript(server2 + "/sites/all/modules/expas/js/expas_quest.js");
			//вопросы
			$('.expas_question').attr('style','color:gray');
			$('#question_' + block).attr('style','color:black').addClass('solid_border');
			if(block) {
				$("#expas_data_question").hide().load(server2 + "/expas_quest/" + block + " #expas_show_quest", function(){
					$.getScript(server2 + "/sites/all/modules/expas/js/expas_value.js");
					//значение
					$('.expas_value').attr('style','color:gray');
					$('#value_' + question).attr('style','color:black').addClass('solid_border');
					if(question) {
						$("#expas_data_value").hide().load(server2 + "/expas_value/" + question + " #expas_show_value").slideDown(1);
					}
				}).slideDown(1);
			}
		}).slideDown(1);
		return 0;
	}	
}