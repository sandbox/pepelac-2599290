(function($){
  Drupal.behaviors.expas = {
	 attach: function(context, settings){
    var server = $("#nodetype-234").attr('value');
		$(".expas_table tr:even").addClass("expas-odd");
		$(".expas_table tr:odd").addClass("expas-even");
		
		$(".expas_nodetype").click(function() {
			//alert('Открываю блоки');
			$('.expas_nodetype').attr('style','color:#999').removeClass('solid_border');
			$(this).attr('style','color:black').addClass('solid_border');
			var id = $(this).attr('value');
			$("#expas_data_nodetype").hide().load(server + "/expas_block/" + id + " #expas_show_block", function(){
				$("#block-add-234").hide();
				$.getScript(server + "/sites/all/modules/expas/js/expas.js");
				$.getScript(server + "/sites/all/modules/expas/js/expas_quest.js");
			}).fadeIn(400);
			return 0;
		});
	 }
  };
})(jQuery);

function expas_hide_show(id) {
		if($('#' + id).is(':visible')) {
			$('#' + id).slideUp(500);
		}
		else {
			$('#' + id).slideDown(500);
		}
		return false;
}
 