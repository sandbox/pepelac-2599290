<?php
/**
 * callback menu function
 * админка модуля
 * 
 */
function expas_admin_place($form, &$form_state) {
//	$check_perm_access_any_role = db_result(db_query("SELECT rid FROM permission WHERE perm LIKE '%expas access evaluation%'"));
	$check_perm_access_any_role = db_query("SELECT rid FROM role_permission WHERE permission LIKE '%expas access evaluation%'")->fetchField();
	$form[] = array(
		'#type' => 'markup',
		'#markup' => '<div class="description">Внимание: для доступа к оцениванию узлов <a href="'.$GLOBALS['base_url'].'/admin/user/permissions">не выбрана</a> ни одна роль.</div>',
		'#access' =>  !$check_perm_access_any_role && user_access('administer site configuration'),
	);
	
	$form['block1'] = array(
		'#type' => 'fieldset',
		'#title' => 'Создать новую форму',
		'#collapsible' => true,
		'#collapsed' => true,
		'#weight' => 0
	);
	$types[''] = 'Выбрать тип узла...';
//	$result = db_query("SELECT nt.type, SUBSTRING(nt.description, 1, 100) as descr FROM {node_type} nt ORDER BY nt.type ASC");
	$result = db_query("SELECT nt.type, SUBSTRING(nt.description, 1, 100) as descr FROM {node_type} nt ORDER BY nt.type ASC")->fetchAll();

//	while($row = db_fetch_object($result))
	foreach ($result AS $row) {
		$types[$row->type] = $row->type . ' [' . strip_tags($row->descr) . ']';
	}
		
	$form['block1']['str_main_chapter'] = array(
		'#type' => 'select',
		'#title' => 'Выбор типа узла, который нужно оценить',
		'#options' => $types,
		'#max_length' => 10,
	);
	$form['block1']['description_main_type'] = array(
		'#type' => 'textarea',
		'#title' => 'Описание',
		'#size' => 2,
	);
	$roles = user_roles();
	$roles[''] = 'Роль не указана...';
	ksort($roles);
	$form['block2'] = array(
		'#type' => 'fieldset',
		'#title' => 'Роль доступа к оценке',
		'#description' => 'Укажите роль, если хотите чтобы пользователь сам смог назначить себе роль для оценки ресурса. Если не указано - пользователь сможет оценивать ресурсы если администратор назначит ему роль (для экспертной оценки).',
		'#weight' => 10004,
		'#collapsible' => true,
		'#collapsed' => true,
		
	);
	$form['block2']['expas_access_role'] = array(
		'#type' => 'select',
		'#weight' => 0,
		'#options' => $roles,
		'#default_value' => variable_get('expas_access_role',''),
		'#description' => 'Впоследствии, пользователи сами смогут назначать себе эту роль.',
	);
	$form['block2']['submit2']  = array(
     	'#type' => 'submit',
		'#name' => 'role_',
		'#value' => 'Сохранить',
		'#submit' => array('expas_list_role_submit'),
		'#weight' => 1,
	);
	
	$form['block1']['expas_parent_node'] = array(
		'#type' => 'textfield',
		'#attributes' => array('style'=>'width:50px'),
		'#title' => 'Id узла',
		'#maxlenght' => 11,
		'#description' => 'Необязательно. Если Вы введете номер узла, то только там появится эта оценочная форма.',
		//'#default_value' => $row->parent_node,
	);
	
	$form['block1']['block01'] = array(
		'#title' => 'Прикрепить к конкурсу',
		'#type' => 'fieldset',
		'#collapsible' => 1,
		'#collapsed' => 1,
		'#access' => module_exists('condes'),
	);
	
	$form['block1']['block01']['search_comp_text'] = array(
		'#title' => 'Строка поиска навания конкурса',
		'#type' => 'textfield',
	);
	$form['block1']['block01']['search_comp_submit'] = array(
		'#type' => 'submit',
		'#prefix' => '<div id="search_comp"></div>',
		'#value' => 'Искать',
		'#ahah' => array(
			'path' => 'expas_search_competition',
			'wrapper' => 'search_comp',
			'effect' => 'fade',
		),
	);
	
	$form['block1']['submit']  = array(
		'#type' => 'submit',
		'#suffix' => '<br />',
		'#value' => 'Добавить',
		'#submit' => array('expas_list_submit'),
		'#weight' => 10004,
	);
	
	
	
	$form['expas_fields'] = array(
		'#type' => 'fieldset',
		'#title' => 'Общие текстовые поля',
		'#description' => 'Текстовые поля которые буду автоматически встраиваться в оценочную форму вместе с вопросами',
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
		'#weight' => 10005,
	);
	
	
	

	
	
	//////
	//собираем общие тектовые поля...
	$fields = expas_admin_place_get_fields();
	if(!empty($form_state['values']['q'])) {
		debug($form_state);
	}
	
	$form['expas_fields']['qqw'] = array(
		'#type' => 'markup',
		'#prefix' => '<div id="expas-block-fields">',
	);
	//...и прикрепляем к массиву
	$form['expas_fields']['all_fields'] = $fields['form'];
	
	
	$form['expas_fields']['expas_fields']['q'] = array(
		'#type' => 'button',
		'#ajax' => array(
			'callback' => 'expas_field_delete',
			'wrapper' => 'expas-block-fields',
			'method' => 'replace',
			'effect' => 'fade',
		),
		'#value' => 'Выполнить операцию с выделенными полями',
		'#attributes' => array('class' => array('add'), 'title' => 'Выполнить действие','return false;'),
		'#weight' => 10006,
		'#suffix' => '<br /><br />',
		'#prefix' => '<br />',
		'#access' => (boolean)$fields['count'],
	);
	
	$form['expas_fields']['field_add'] = array(
			'#prefix' => '<div class="container-inline">',
			'#type' => 'textfield',
			'#size' => 100,
			'#weight' => 10007,
	);
	
	$form['expas_fields']['field_add_press'] = array(
		'#type' => 'button',
		'#value' => ' + ',
		'#attributes' => array('class' => array('add')),
		'#ajax' => array(
			'callback' => 'expas_field_add',
			'wrapper' => 'expas-block-fields',
			'method' => 'replace',
			'effect' => 'fade',
		),
		'#suffix' => '</div>',
		'#weight' => 10008,
	);
	
	$form['expas_fields']['a'] = array(
		'#type' => 'markup',
		'#markup' => '</div>',
		'#weight' => 10009,
	);
	
	
	///////
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	$form['block3'] = array(
	 '#type' => 'fieldset',
	 '#title' => 'Условия отображения форм',
	 '#description' => '<strong>Условие показа</strong> - укажите метод, возвращающий TRUE или FALSE. В качестве аргумента будет взят объект node текущего узла. Если пусто - отображается в обычном порядке.<br /><strong>Отношение</strong> - укажите метод, возвращающий массив идентификаторов узлов к которым будет относится каждый эксперт. Если путо - все узлы для экспертной оценки.<br /><strong>Callback</strong> - постобработка результатов экспертизы.',
	 '#collapsed' => @!$_POST['sql_condition'],
	 '#collapsible' => TRUE,
	 '#weight' => 9000,
	);
	
	$form['block3'][] = array(
	 '#type' => 'markup',
	 '#markup' => '<table><thead><tr><th>Тип узла</th><th>Блоков</th><th>Условие показа</th><th>Отношение</th><th>Callback</th><th>Включен</th><th>Ссылка<span title="Внедрить в тело документа ссылку на форму экспертизы (аналог вкладки)">[?]</span></th><th>Просмотр<span title="Отображать вкладку с готовыми оценками">[?]</span></th></tr><tbody>',
	 '#weight' => 0,
	);
//	$result = db_query("SELECT ebn.*, (SELECT COUNT(*) FROM {expas_blocks} WHERE id_form_type_node = ebn.cid) as `cnt` FROM {expas_type_node} ebn");
	$result = db_query("SELECT ebn.*, (SELECT COUNT(*) FROM {expas_blocks} WHERE id_form_type_node = ebn.cid) as `cnt` FROM {expas_type_node} ebn")->fetchAll();
	$opt = array('Выбор...');
//	while($row = db_fetch_object($result)) {
	foreach($result AS $row) {
	  $form['block3'][] =  array(
	   '#type' => 'markup',
	   '#prefix' => '<tr class="odd"><td>',
	   '#suffix' => '</td>',
	   '#markup' => l($row->type_node, $GLOBALS['base_url'].'/admin/settings/expas/showform/'.$row->cid),
	   '#weight' => $row->cid,
	  );
	  $form['block3'][] =  array(
	   '#type' => 'markup',
	   '#prefix' => '<td>',
	   '#suffix' => '</td>',
	   '#markup' => $row->cnt,
	   '#weight' => $row->cid + 0.01,
	  );
	  
	  if($row->method_condition && !function_exists($row->method_condition)) {
	  	$css_err = ' color:red; border:1px solid red;';
	  }
	  elseif(!$row->method_condition) {
	  	$css_err = '';
	  }
	  else {
	  	$css_err = ' color:green';
	  }
	  
	  $form['block3']['sql_conditions['.$row->cid.']'] = array(
	   '#type' => 'textfield',
	   '#attributes' => array('style' => 'width:120px;'.$css_err),
	   '#prefix' => '<td>',
	   '#value' => $row->method_condition,
	   '#weight' => $row->cid + 0.1,
	   '#suffix' => '</td>',
	  );
	  
	  if($row->expert_relation && !function_exists($row->expert_relation)) {
	  	$css_err2 = ' color:red; border:1px solid red;';
	  }
	  elseif(!$row->expert_relation) {
	  	$css_err2 = '';
	  }
	  else {
	  	$css_err2 = ' color:green';
	  }
	  
	  
	  $form['block3']['expert_relation['.$row->cid.']'] = array(
	   '#type' => 'textfield',
	   '#attributes' => array('style' => 'width:120px;'.$css_err2),
	   '#prefix' => '<td>',
	   '#value' => $row->expert_relation,
	   '#weight' => $row->cid + 0.11,
	   '#suffix' => '</td>',
	  );
	  
	  if($row->method_callback && !function_exists($row->method_callback)) {
	  	$css_err3 = ' color:red; border:1px solid red;';
	  }
	  elseif(!$row->method_callback) {
	  	$css_err3 = '';
	  }
	  else {
	  	$css_err3 = ' color:green';
	  }
	  
	  
	  $form['block3']['method_callback['.$row->cid.']'] = array(
	   '#type' => 'textfield',
	   '#attributes' => array('style' => 'width:120px;'.$css_err3),
	   '#prefix' => '<td>',
	   '#value' => $row->method_callback,
	   '#weight' => $row->cid + 0.12,
	   '#suffix' => '</td>',
	  );
	  $form['block3']['form_enabled['.$row->cid.']'] = array(
	   '#type' => 'checkbox',
	   '#prefix' => '<td>',
	   '#default_value' => $row->enabled,
	   '#weight' => $row->cid + 0.2,
	   '#suffix' => '</td>',
	  ); 
	  $form['block3']['form_body_link['.$row->cid.']'] = array(
	   '#type' => 'checkbox',
	   '#prefix' => '<td>',
	   '#default_value' => $row->body_link,
	   '#weight' => $row->cid + 0.3,
	   '#suffix' => '</td>',
	  ); 
	  $form['block3']['form_layout_list_evals['.$row->cid.']'] = array(
	   '#type' => 'checkbox',
	   '#prefix' => '<td>',
	   '#default_value' => $row->layout_list_evals,
	   '#weight' => $row->cid + 0.4,
	   '#suffix' => '</td></tr>',
	  );
	}
	$form['block3']['submit'] = array(
	 '#type' => 'submit',
	 '#name' => 'cond',
	 '#value' => 'Сохранить',
	 '#submit' => array('expas_condition_submit'),
	 '#weight' => 50,
	 '#prefix' => '<tr><td colspan="2">',
	 '#suffix' => '</td></tr>',
	);
	$form['block3'][] = array(
	 '#type' => 'markup',
	 '#suffix' => '</tbody></table>',
	 '#weight' => 51,
	);
	return $form;
}
/**
 * создание формы для настройки общих текстовых полей
 * @return array - form -форма, count - кол-во записей
 */
function expas_admin_place_get_fields() {
	$result = db_query("SELECT * FROM {expas_fields}")->fetchAll();
  $form = array();
	foreach ( $result AS $row ) {
		$form['expas_fields']['fields['.$row->fid.']'] = array(
			'#prefix' => '<div class="container-inline">',
			'#type' => 'textfield',
			'#size' => 100,
			'#default_value' => $row->field,
			'#weight' => $row->fid,
		);
		$form['expas_fields']['update_fields['.$row->fid.']'] = array(
			'#type' => 'checkbox',
			'#title' => 'обновить',
			'#weight' => $row->fid + 0.1,
		);
		$form['expas_fields']['delete_fields['.$row->fid.']'] = array(
			'#type' => 'checkbox',
			'#title' => 'удалить',
			'#weight' => $row->fid + 0.2,
			'#suffix' => '</div>',
		);
	}
	return array('form' => $form, 'count' => (int)sizeof($form));
}
/**
 * Ajax callback form
 * удаление или операция с общим текстовым полем
 * @param unknown_type $form
 * @param unknown_type $form_state
 */
function expas_field_delete($form, $form_state) {
	
}
/**
 * AJAX callback form
 * добавление записей общих тектосвых форм
 */
function expas_field_add($form, $form_state) {
	if(@$form_state['values']['field_add']) {
		expas_fields_add($form_state['values']['field_add']);
	}
	//$fields = expas_admin_place_get_fields();
	//$form['expas_fields']['all_fields'] = $fields['form'];
	//$form['expas_fields']['expas_fields']['q']['#access'] = (boolean)$fields['count'];
	return $form['expas_fields']['all_fields'];
}


/**
 * Обработчик нажатия кнопки 
 * условия отображения
 */
function expas_condition_submit($form, &$form_state) {
  if(is_array($form_state['input']['sql_conditions'])) {
//    db_query("UPDATE {expas_type_node} SET method_condition = ''");
    db_update("expas_type_node")
    ->fields(array(
    	'method_condition' => '',
    ))->execute();
    foreach($form_state['input']['sql_conditions'] AS $key => $value) {
//      db_query("UPDATE {expas_type_node} SET method_condition = '%s' WHERE cid = %d", strip_tags(trim($value)), $key); 
      db_update("expas_type_node")
      ->fields(array(
      	'method_condition' => strip_tags(trim($value))
      ))
      ->condition('cid', $key)
      ->execute(); 
    }
  }
//  db_query("UPDATE {expas_type_node} SET body_link = 0");
  db_update("expas_type_node")->fields(array('body_link' => 0))->execute();
  //debug($form_state['input']['form_body_link']);
  if(is_array($form_state['input']['form_body_link'])) {
    foreach($form_state['input']['form_body_link'] AS $key => $value) {
//      db_query("UPDATE {expas_type_node} SET body_link = %d WHERE cid = %d", $value, $key); 
      db_update("expas_type_node")
      ->fields(array(
      	'body_link' => $value,
      ))
      ->condition('cid', $key)
      ->execute();
    }
  } 
//  db_query("UPDATE {expas_type_node} SET layout_list_evals = 0");
  db_update("expas_type_node")->fields(array('layout_list_evals' => 0))->execute();
  if(is_array($form_state['input']['form_layout_list_evals'])) {
    foreach($form_state['input']['form_layout_list_evals'] AS $key => $value) {
//      db_query("UPDATE {expas_type_node} SET layout_list_evals = %d WHERE cid = %d", $value, $key); 
      db_update("expas_type_node")->fields(array('layout_list_evals' => $value))->condition('cid', $key)->execute(); 
    }
  }
//  db_query("UPDATE {expas_type_node} SET expert_relation = ''");
  db_update("expas_type_node")->fields(array('expert_relation' => ''))->execute();
  if(is_array($form_state['input']['expert_relation'])) {
    foreach($form_state['input']['expert_relation'] AS $key => $value) {
//      db_query("UPDATE {expas_type_node} SET expert_relation = '%s' WHERE cid = %d", $value, $key); 
      db_update("expas_type_node")->fields(array('expert_relation' => $value))->condition('cid', $key)->execute(); 
    }
  }
//  db_query("UPDATE {expas_type_node} SET method_callback = ''");
  db_update("expas_type_node")->fields(array('method_callback' => ''))->execute();
  if(is_array($form_state['input']['method_callback'])) {
    foreach($form_state['input']['method_callback'] AS $key => $value) {
//      db_query("UPDATE {expas_type_node} SET method_callback = '%s' WHERE cid = %d", $value, $key); 
      db_update("expas_type_node")->fields(array('method_callback' => $value))->condition('cid', $key)->execute(); 
    }
  }
  if(is_array($form_state['input']['form_enabled'])) {
//    db_query("UPDATE {expas_type_node} SET enabled = 0");
    db_update("expas_type_node")->fields(array('enabled' => 0))->execute();
    foreach($form_state['input']['form_enabled'] AS $key => $value) {
//      db_query("UPDATE {expas_type_node} SET enabled = %d WHERE cid = %d", $value, $key); 
      db_update("expas_type_node")->fields(array('enabled' => $value))->condition('cid', $key)->execute(); 
    }
  }
  //проверка на включенность более 1 форм для одного типа узла
  $check_result = db_query("SELECT COUNT(*) AS `cnt`, `type_node` FROM {expas_type_node} WHERE enabled = 1 GROUP BY `type_node`")->fetchAll();
//  while($row = db_fetch_object($check_result)) {
  foreach($check_result AS $row) {
  	if($row->cnt > 1) {
  		drupal_set_message('Включено должно быть не более 1-ой оценочной формы для одного типа узла.<br />Все формы для типа узла - "'.$row->type_node.'" были выключены.','error');
//  		db_query("UPDATE {expas_type_node} SET enabled = 0 WHERE type_node = '%s'", $row->type_node);		
  		db_update("expas_type_node")
  		->fields(array(
  			'enabled' => 0
  		))
  		->condition('type_node', $row->type_node)
  		->execute();		
  	}
  }
}


/**
 * просмотр готовой	 формы
 *
 * @param int $id
 * @return str
 */
function expas_constructor_showform(){
	$args = func_get_args();
	if(!$args[0]){
		return '<div>Нажмите на "Просмотр формы" в разделе "Конструктор"';
	}
//	$node = db_fetch_object(db_query("SELECT nt.type FROM {node_type} nt, {expas_type_node} etn WHERE etn.cid = %d && etn.type_node = nt.type", $args[0]));
	$node = db_query("SELECT nt.type FROM {node_type} nt, {expas_type_node} etn WHERE etn.cid = :cid && etn.type_node = nt.type", array(':cid' => $args[0]))->fetchObject();
	return theme('expas_evaluation_form_test', array('node' => $node));
}
/**
 * обработчик нажатия кнопки внести новый раздел
 */
function expas_list_submit(&$form, $form_state) {
	global $user;
	if($form_state['values']['str_main_chapter']) {
		if(@$form_state['values']['competition_nid'] > 0) {
//			$check_query = db_result(db_query("SELECT сid FROM {expas_type_node} WHERE competition_nid = %d LIMIT 1", $form_state['values']['competition_nid']));
			$check_query = db_query("SELECT сid FROM {expas_type_node} WHERE competition_nid = :nid LIMIT 1", array(':nid' => $form_state['values']['competition_nid']))->fetchField();
			if($check_query > 0) {
				drupal_set_message('К сожалению, выбранный Вами конкурс уже используется для '.l('другой оценочной формы.', $GLOBALS['base_url'].'/admin/settings/expas/showform/'.$check_query), 'error');
				return 0;
			}
		}
		elseif($form_state['values']['expas_parent_node'] > 0){ # привязанный к конкретному узлу.
//			$check_query = db_result(db_query("SELECT nid FROM {node} WHERE type = '%s' && nid = %d", $form_state['values']['str_main_chapter'], $form_state['values']['expas_parent_node']));
			$check_query = db_query("SELECT nid FROM {node} WHERE type = :type && nid = :nid", array(':type' => $form_state['values']['str_main_chapter'], ':nid' => $form_state['values']['expas_parent_node']))->fetchField();
			if(!$check_query){
				drupal_set_message('К сожалению, введенный Вами Id узла не принадлежит к тому типу контента, который Вы выбрали.', 'error');
				return 0;
			}
			else {
//				$check_query = db_result(db_query("SELECT cid FROM {expas_type_node} WHERE type_node = '%s' && parent_node = %d", $form_state['values']['str_main_chapter'], $form_state['values']['expas_parent_node']));
				$check_query = db_query("SELECT cid FROM {expas_type_node} WHERE type_node = :type && parent_node = :parent", array(':type' => $form_state['values']['str_main_chapter'], ':parent' => $form_state['values']['expas_parent_node']))->fetchField();
				if($check_query){
					drupal_set_message('Ошибка. Уже существует форма привязанная к данному типу узла и введеному id.','error');
					return 0;
				}
			}
		}
//		elseif($GLOBALS['_POST']['add_competition'] < 1) { # не привязанный к конкретному узлу
//			$check_query = db_result(db_query("SELECT cid FROM {expas_type_node} WHERE type_node = '%s' && parent_node = 0 && competition_nid = 0", $form_state['values']['str_main_chapter']));
//			if($check_query){
//				drupal_set_message('Ошибка. Уже существует форма привязанная к данному типу узла, не ассоциированная с конкретным узлом (node id) или конкурсом. Вы можете указать только конкретный узел (node id) c введенным Вами типом узла или конкурс.','warning');
//				return 0;
//			}
//		}
//		db_query("INSERT INTO {expas_type_node} SET type_node = '%s', description = '%s', uid = %d, parent_node = %d, competition_nid = %d", $form_state['values']['str_main_chapter'], $form_state['values']['description_main_type'],$user->uid, $form_state['values']['expas_parent_node'], $GLOBALS['_POST']['add_competition']);
		$last_id = db_insert("expas_type_node")
		->fields(array(
			'type_node' => $form_state['values']['str_main_chapter'],
			'description' => $form_state['values']['description_main_type'],
			'uid' => $user->uid,
			'parent_node' => intval($form_state['values']['expas_parent_node']),
			'competition_nid' => @$GLOBALS['_POST']['add_competition'],
		))->execute();
		//$last_id = db_fetch_object(db_query("SELECT LAST_INSERT_ID() as id"));
//		db_query("UPDATE {expas_type_node} SET id_form = %d WHERE cid = %d", $last_id->id,$last_id->id);
		db_update("expas_type_node")
		->fields(array(
			'id_form' => $last_id,
		))
		->condition('cid', $last_id)
		->execute();
		variable_set('expas_'.$form_state['values']['str_main_chapter'].'_enabled', 1);
		drupal_set_message('Новый тип узла был добавлен');
		drupal_goto($GLOBALS['base_url'].'/admin/settings/expas/constructor');
	}
}
/**
 * обработчик нажатия кнопки роль доступа к оценке
 */
 
function expas_list_role_submit($form, &$form_state) {
	variable_del('expas_access_role');
	if($form_state['values']['expas_access_role']) {
		variable_set('expas_access_role', $form_state['values']['expas_access_role']);
		drupal_set_message('Роль была сохранена');
	}
}
/**
 * вывод типов узлов
 *
 * @return unknown
 */
function expas_constructor_questions() {
	if($GLOBALS['_POST']) {
	  //debug($GLOBALS['_POST']);
		expas_delete_value_submit(); expas_add_value_submit(); expas_add_question(); expas_delete_question(); expas_add_block(); expass_delete_block();
		drupal_add_js(drupal_get_path('module','expas').'/js/expand_three.js');
		drupal_add_js('expas_show_all('.$GLOBALS['_POST']['nodetype'].','.$GLOBALS['_POST']['block'].','.$GLOBALS['_POST']['question'].');','inline','footer');
	}
	
	drupal_set_title('Expas: Конструктор форм.');
	if(@is_array($GLOBALS['_POST']['expas_delete_nodetype'])) {
		foreach ($GLOBALS['_POST']['expas_delete_nodetype'] AS $key => $value) {
			if($value == 'on') {
//				$nodetype = db_fetch_object(db_query("SELECT (SELECT COUNT(*) FROM {expas_type_node} etn2 WHERE etn2.type_node = etn.type_node) AS `cnt`, etn.type_node FROM {expas_type_node} etn WHERE etn.cid = %d", $key));
				$nodetype = db_query("SELECT (SELECT COUNT(*) FROM {expas_type_node} etn2 WHERE etn2.type_node = etn.type_node) AS `cnt`, etn.type_node FROM {expas_type_node} etn WHERE etn.cid = :cid", array(':cid' => $key))->fetchObject();
				//db_query("DELETE FROM {expas_type_node} WHERE cid = %d", $key);
				db_delete("expas_type_node")
				->condition('cid', $key)
				->execute();
				if($nodetype->cnt == 1) {
					variable_del('expas_'.$nodetype->type_node.'_enabled');
				}
			}
		}
	}
	
//	$query = "SELECT * FROM {expas_type_node} ORDER BY cid ASC";
	$types = db_select('expas_type_node', 'etn')
	  ->fields('etn')
		->extend('PagerDefault')
		->limit(20)
		->execute();
	$count = 0;
	
	//$result = pager_query($query, 20, 0);
//	while($row = db_fetch_object($result)) {
	foreach ($types AS $row) {
//		$cnt_chapters = db_result(db_query("SELECT COUNT(*) AS cnt FROM {expas_blocks} WHERE id_form_type_node = %d", $row->id_form));
		$cnt_chapters = db_query("SELECT COUNT(*) AS cnt FROM {expas_blocks} WHERE id_form_type_node = :type", array(':type' => $row->id_form))->fetchField();
		$class_color = ($row->parent_node > 0) ? 'expas_set_color_node' : false;
		
		if($row->competition_nid > 0) {
//			$title = db_result(db_query("SELECT SUBSTRING(title,1,30) FROM {node} WHERE nid = %d && status = 1", $row->competition_nid));
			$title = db_query("SELECT SUBSTRING(title,1,30) FROM {node} WHERE nid = :nid && status = 1", array(':nid' => $row->competition_nid))->fetchField();
			$parent_node = '<fieldset><legend>Привязка к конкурсу</legend>';
			if(!$title) 
				$parent_node .= l('Конкурс не существует', $GLOBALS['base_url'].'/node/'.$row->competition_nid);
			else 
				$parent_node .= l($title, $GLOBALS['base_url'].'/node/'.$row->competition_nid);
			$parent_node .= '</fieldset>';	
		}
		elseif($row->parent_node > 0) {
//			$title = db_result(db_query("SELECT SUBSTRING(title,1,30) FROM {node} WHERE nid = %d && status = 1", $row->parent_node));
			$title = db_query("SELECT SUBSTRING(title,1,30) FROM {node} WHERE nid = :nid && status = 1", array(':nid' => $row->parent_node))->fetchField();
			$parent_node = '<fieldset><legend>Привязка к узлу</legend>';
			if(!$title) 
				$parent_node .= l('Узел не существует', $GLOBALS['base_url'].'/node/'.$row->parent_node);
			else 
				$parent_node .= l($title, $GLOBALS['base_url'].'/node/'.$row->parent_node);
			$parent_node .= '</fieldset>';
		}
		else {
			$parent_node = false;
		}
		
		$form[] = array(
			'#type' => 'markup',
			'#markup' => '<div class="expas_nodetype" value="'.$row->cid.'" id="nodetype_'.$row->cid.'"><h2 class="'.$class_color.'">'.$row->type_node.'</h2> блоков: '.$cnt_chapters."\r\n".' '.(($row->enabled) ? '&nbsp; &nbsp; <span style="color:green">Включен</span>' : '&nbsp; &nbsp; <span class="error">Выключен</span>').'  <div class="description">'.$row->description.'</div>'.$parent_node.l('Просмотр формы', $GLOBALS['base_url'].'/admin/settings/expas/showform/'.$row->cid).'</div>'."\r\n".
			'<input type="checkbox" name="expas_delete_nodetype['.$row->cid.']">'."\r\n",
			'#weight' => $row->cid,	
		);
		$count++;
	}
	
	if(!$count) {
		$form['q'] = array(
			'#type' => 'markup',
			'#markup' => 'Данных нет',
		);
	}
	
	$form['submit'] = array(
		'#type' => 'submit',
		'#access' => $count > 0,
		'#value' => ' -- ',
		'#weight' => 1000000,
		'#attributes' => array('onClick' => 'return confirm("Удалить выделенный узел")', 'class' => array('delete')),
	);
		
	$htm = "\r\n".'<form id="nodetype-234" method="post" action="" accept-charset="UTF-8" value="'.$GLOBALS['base_url'].'">'."\r\n";
	$htm .= '<TABLE cellpadding="4" cellspacing="4" class="solid_border">'."\r\n".'<tr><td class="top">'."\r\n";
	//$form_state = array();
	//$form = form_builder('block-234' , $form, $form_state);
	$htm .= drupal_render($form);
	$htm .= '</td><td class="top"><div id="expas_data_nodetype"></div></td></tr>'."\r\n".'</TABLE>'."\r\n";
	$htm .= '</form>'."\r\n";
	
	return $htm;
}
/**
 * выводим блоки вопросов узла
 *
 * @return str
 */
function expas_show_block(){
	$args = func_get_args();
	return theme('expas_theme_block', array('args' => $args));
}
/**
 * выводим вопросы блока
 *
 * @return str
 */
function expas_show_quest(){
	$args = func_get_args();
	return theme('expas_theme_quest', $args);
}
/**
 * вывод значений для вопроса
 *
 * @return str
 */
function expas_show_value(){
	$args = func_get_args();
	return theme('expas_theme_value', $args);
}
/**
 * обработчик нажатия кнопки добавить новое значение для вопроса
 */
function expas_add_value_submit() {
	if(is_array($GLOBALS['_POST']['value_add'])) {
		foreach ($GLOBALS['_POST']['value_add'] AS $key => $value) {
			if($value != '') {
				//if(db_query("INSERT INTO {expas_values} SET eid = %d, value = %d, description = '%s'", $key, $value, $GLOBALS['_POST']['value_add_descr'][$key]))
				$id = db_insert("expas_values")
					->fields(array(
						'eid' => $key,
						'value' => $value,
						'description' => $GLOBALS['_POST']['value_add_descr'][$key],
					))->execute();	
					if($id) {				
						drupal_set_message('значение было добавлено');
					}
			}
		}
	}
}
/**
 * Добавление вопроса
 *
 */
function expas_add_question(){
	if($GLOBALS['_POST']['expas_title_question']) {
		$id = time();
//		if(db_query("INSERT INTO {expas_questions} SET form_name = '%s', title = '%s', description = '%s', chapter = %d", 'type'.$id, strip_tags($GLOBALS['_POST']['expas_title_question']), strip_tags($GLOBALS['_POST']['expas_descr_question']), $GLOBALS['_POST']['block'])) {
		$id = db_insert("expas_questions")
			->fields(array(
				'form_name' => 'type'.$id,
				'title' => strip_tags($GLOBALS['_POST']['expas_title_question']),
				'description' => strip_tags($GLOBALS['_POST']['expas_descr_question']),
				'chapter' => $GLOBALS['_POST']['block'],
			))->execute();
			
			if($id) {
				drupal_set_message('вопрос был добавлен');
			}
	}
}
/**
 * Удаление вопроса
 *
 */
function expas_delete_question() {
	if(is_array($GLOBALS['_POST']['expas_delete_question'])) {
		foreach ($GLOBALS['_POST']['expas_delete_question'] AS $key => $value) {
			if($value == 'on') {
				//if(db_query("DELETE FROM {expas_questions} WHERE eid = %d", $key)) {
				$id = db_delete('expas_questions')
					->condition('eid', $key)
					->execute();
					if($id) {
						drupal_set_message('вопрос был удален');
					}
			}
		}
	}
}
/**
 * Добавление нового блока
 */
function expas_add_block(&$form, &$form_state) {
//	if($GLOBALS['_POST']['expas_title_block']) {
	if($form_state['values']['expas_title_block']) {
	  
		//if(db_query("INSERT INTO {expas_blocks} SET title = '%s', id_form_type_node = %d", strip_tags($GLOBALS['_POST']['expas_title_block']), $GLOBALS['_POST']['nodetype']))
		$id = db_insert("expas_blocks")
			->fields(array(
				'title' => strip_tags($GLOBALS['_POST']['expas_title_block']),
				'id_form_type_node' => $GLOBALS['_POST']['nodetype'],
			))->execute();
		if($id) {
			drupal_set_message('новый блок для вопросов был удачно добавлен');
		}
	}
}
/**
 * Удаление блока
 */
function expass_delete_block() {
	if(is_array($GLOBALS['_POST']['expas_delete_block'])) {
		foreach ($GLOBALS['_POST']['expas_delete_block'] AS $key => $value) {
			if($value == 'on') {
				//if(db_query("DELETE FROM {expas_blocks} WHERE category_id = %d", $key))
				$id = db_deletey("expas_blocks")
					->condition('category_id', $key)
					->execute();
				if($id) {	
					drupal_set_message('блок был удален');
				}
			}
		}
	}
}
/**
 * Обработчик для кнопки удаления значения
 */
function expas_delete_value_submit() {
	//$args = func_get_args();
	if(is_array($GLOBALS['_POST']['value_delete'])) {
		foreach ($GLOBALS['_POST']['value_delete'] AS $key => $value){
			foreach ($value AS $key2 => $value2) {
				if($value2 == 1) {
//					if(db_query("DELETE FROM {expas_values} WHERE id = %d && eid = %d", $key2, $key))
					$id = db_delete("expas_values")
						->condition('id', $key2)
						->condition('eid', $key)
						->execute();
					if($id) {	
						drupal_set_message('значение было удалено');
					}
				}
			}
		}
	}
}





