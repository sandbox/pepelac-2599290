<?php
/**
 * @file
 * набор функций для работы с экспертной оценкой
 */

/**
 * собираем заявки к которым прикреплен эксперт
 * эта функция указана в настройках модуля - Отношение
 *
 * @return array
 */
function expas_expert_get_zayavka(){
	global $user;
	$args = func_get_args();
	if(user_access('expas moderator')) {
		$uid = $args[0]->uid;
	}
	elseif($user->uid == $args[0]->uid) {
		$uid = $user->uid;
	}
	else {
		return array();
	}
	$query = "SELECT nid FROM {content_field_zayavka_experts} WHERE field_zayavka_experts_value = %d";
	$result = db_query($query, $uid);
	$nids = array();
	while($nid = db_result($result)) {
		$nids[] = $nid;
	}
	return $nids;
}

/**
 * Возвращает список uid экспертов, прикрепленных к заявке
 */
function expas_get_experts_from_nid($nid) {
  $q = db_query('SELECT field_zayavka_experts_value FROM {content_field_zayavka_experts} WHERE nid = %d', $nid);
  $experts = array();
  while ($result = db_result($q)) {
    $experts[] = $result;
  }
  return $experts;
}

/**
 * Список экспертов с фио и с ограничением доступа на просмотр
 */
function expas_get_experts_from_nid_themed($nid) {
  if (user_access('view experts list')) {
  $experts = expas_get_experts_from_nid($nid);
  $list = array();
  foreach ($experts as $uid) {
    $account = user_load($uid);
    $list[] = _expas_get_fio($account);
  }
  return implode('<br/>', $list);
  }
  return FALSE;
}
/**
 * получаем ФИО эксперта из профиля
 * если не определены - возвращаем имя
 *
 * @param obj $account - объект пользователя
 * @return str
 */
function _expas_get_fio(&$account) {
  $fields = array('field_profile_family', 'field_profile_name', 'field_profile_secondname');
  $fio = array();
  foreach ($fields AS $cck_field) {  
    $field_data = field_view_field('user', $account, $cck_field);
    $fio[] = render($field_data); 
  }
  if(sizeof($fio) > 0) {
    return explode(" ", $fio);  
  }
  else  {
    return $account->name;
  }
}

/**
 * если эксперт прикреплен к заявке - показываем ему форму
 * указано в настройках модуля - Условие показа
 *
 * @return boolean
 */
function expas_expert_show_form(){
	global $user;
	return (boolean) db_result(db_query("SELECT COUNT(*) FROM {content_field_zayavka_experts} WHERE field_zayavka_experts_value = %d", $user->uid));
}
/**
 * отображает значения направления программ для отображения печати экспертизы конкретного эксперта
 *
 * @param int $id
 * @param str $pretitle - программа или направление
 * @return str
 */
function expas_form_show_direction($category, $id) {
	$data[6] = array(
		1 => 'Программа не заслуживает поддержки',
		2 => 'Программа может быть поддержана',
		3 => 'Программа заслуживает поддержки',
	);
	$data[7] = array(
		0 => 'Направление не заслуживает поддержки',
		1 => 'Направление не заслуживает поддержки',
		2 => 'Направление может быть поддержано',
		3 => 'Направление заслуживает поддержки',
	);
	return $data[$category][$id];
}
/**
 * узнаем вес для вопроса
 *
 * @param int $question_id
 * @return int
 */
function expas_form_get_coef($question_id){
	static $data;
	if(!$data) {
		$data = array(
			1 => 0.2,
			2 => 0.2,
			3 => 0.1,
			4 => 0.1,
			//6 => 1,
			7 => 0.1,
			8 => 0.1,
			9 => 0.1,
			10 => 0.1,
			/*15 => 1,
			16 => 1,
			17 => 1,
			18 => 1,
			19 => 1,
			20 => 1,
			21 => 1,
			22 => 1,
			23 => 1,
			24 => 1,*/
		);
	}
	return $data[$question_id];
}
/**
 * Формула 1 - отображение
 *
 * @param int $ratio
 * @return int
 */
function expas_get_value_formula_1($ratio) {
	$ratio = floatval($ratio);
	if ($ratio > 0.5) {
		$value = 100;
	}
	elseif ( $ratio >= 0.26 && $ratio <= 0.5 ) {
		$delta = $ratio - 0.25;
		$delta *= 100;
		$delta = round($delta);
		$delta *= 2;
		$value = 50 + $delta;
	}
	elseif ( $ratio == 0.25 ) {
		$value = 50;
	}
	elseif ( $ratio < 0.25 ) {
		$value = 0;
	}
	else {	
	 $value = 0;	
	}
	return $value;
}
/* расчитываем значение "запрашиваемого размера субсидии на реализацию Программы'  */
function expas_get_value_formula_2014_prepare($programma_nid) {
	return db_result(db_query("SELECT ROUND(sum(field_event_raised_funds_value)/sum(e.field_event_requestsum_value)*100) FROM {content_type_event} e WHERE e.field_event_programmanid_value = %d", $programma_nid));
}

/*расчитываем значение для первого вопроса по формуле 
1, 1. Объем софинансирования Программы за счет собственных и привлеченных средств

Свыше 50% запрашиваемого размера субсидии на реализацию Программы – 100 баллов.
От 26 до 50% – 50 баллов плюс 2 балла за каждый процент свыше 25%.
25% – 50 баллов.
Менее 25% – 0 баллов.

@node - программа
*/
function expas_get_value_formula_2014($programma_nid) {
	//% запрашиваемого размера субсидии на реализацию Программы
	$data = expas_get_value_formula_2014_prepare($programma_nid);//% временные данные 
	//print $data;
	if ($data >= 51) {
		$value = 100;
	}
	else if ($data >= 26 && $data <= 50) {
		$delta = $data - 25;
		$value = 50 + ($delta*2);
	}
	else if ($data == 25) {
		$value = 50;
	}
	else if ($data < 25) {
		$value = 0;
	}
	return $value;
}



/*print expas_get_value_formula_1(0.28);*/

/**
 * подсчет по формуле 2
 *
 * @param int $zayavka_nid
 * @return int
 */
function expas_get_value_formula_2($zayavka_nid){
return 0;
	$query = "SELECT ROUND((SELECT COUNT(*) FROM {content_type_event} e
							INNER JOIN {content_field_zayavka_link} z ON z.nid = e.nid
							WHERE z.field_zayavka_link_nid = %d && e.field_integration_value is not null)/(SELECT COUNT(*) FROM {content_type_event} e
							INNER JOIN {content_field_zayavka_link} z ON z.nid = e.nid
							WHERE z.field_zayavka_link_nid = %d),2)";
	
	$koef = db_result(db_query($query, $zayavka_nid, $zayavka_nid));	
	$koef = floatval($koef);
	
	if($koef >= 0.1){
		$value = 100;
	}
	elseif ( $koef >= 0.06 && $koef <= 0.09 ) {
		$value = 60;
	
	}
	elseif ( $koef >= 0.01 && $koef <= 0.05 ) {
		$value = 40;
	}
	else {
		$value = 0;			
	}
	return $value;
}
/**
 * встраиваем в среднюю оценку исключения по некоторым вопросам
 *
 * @return str
 */
function expas_average_ball_between(){
	return '&& v.eid NOT BETWEEN 9 AND 20';
}
/**
 * кнопка для скачки экспертных оценок по заявке
 * @param obj $node
 * @return array
 */
function expas_print_one_zayavka($post, $node){
	$form['block'] = array(
		'#type' => 'fieldset',
		'#title' => 'Распечатать',
		'#collapsible' => 1,
		'#collapsed' => 1,
	);
	$form['block']['excel'] = array(
		'#type' => 'checkbox',
		'#default_value' => 0,
		'#title' => 'В Excel',
	);
	$form['block']['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Получить HTML',
	);
	$form['block']['zayavka_nid'] = array(
		'#type' => 'hidden',
		'#value' => $node->nid,
	);
	return $form;
}
/**
 * callback function
 */
function expas_print_one_zayavka_submit(&$form, &$form_state) {
	$zayavka = node_load($form_state['values']['zayavka_nid']);
	$args = array();
	$args['title'] = $zayavka->title;
	$args['code'] = _expas_get_zayavka_code($zayavka);
	$args['zayavka_nid'] = $zayavka->nid;
	//VUZ
	$organisation_nid  = db_result(db_query("SELECT cfz.nid FROM {content_field_zayavka_link} cfz INNER JOIN {node} n ON cfz.nid = n.nid AND cfz.field_zayavka_link_nid = %d AND n.type='org'", $zayavka->nid));
  $organisation_data = node_load($organisation_nid);
	
  $args['vuz_name'] = $organisation_data->title;
  
  
  if($form_state['values']['excel']) {
	  header('Content-Type: text/html; charset=utf-8');
		header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0', FALSE);
		header('Pragma: no-cache');
		header('Content-transfer-encoding: binary');
		header('Content-Disposition: attachment; filename=zayavka-node-'.$node->nid.'.xls');
		header('Content-Type: application/x-unknown');
  }
  print theme('expas_one_zayavka_excel', $args);
	exit();
}
/**
 * узнаем все оценки по позициям в кокретной заявке
 * конкретного эксперта. 
 *
 * @param int $expert_uid
 * @param int $zayavka_nid
 * @return array
 */
function expas_expert_ever_values($expert_uid, $zayavka_nid) {
	$query = "SELECT SUM(value) as `sum`, eid FROM {expas_data_values} WHERE uid=:uid && nid=:nid GROUP BY eid ORDER BY eid ASC";
	$result = db_query($query, array(':uid' => $expert_uid, ':nid' => $zayavka_nid))->fetchAll();
	$vals = array();
	foreach ($result AS $row) {
		//if(!in_array($row->eid, array(3,6))) {
			//$koef = expas_form_get_coef($row->eid);
			//$row->sum *= $koef;
		//}
		$vals[$row->eid] = round($row->sum, 1);
	}
	//expas_dpr($vals);
	return array_sum($vals);
}
/**
 * узнаем все оценки по позициям в кокретной заявке
 * @param int $zayavka_nid
 * @return array
 */
function expas_zayavka_all_values($zayavka_nid) {
	$query = "SELECT COUNT(*), uid FROM {expas_data_values} v2 WHERE nid = %d GROUP BY uid";
	$result = db_query($query, $zayavka_nid);
	$count = 0;
	$value_expert = array();
	while($row = db_fetch_object($result)) {
		$value_expert[] = expas_expert_ever_values($row->uid, $zayavka_nid);
		$count++;
	}
	
	if(sizeof($value_expert) > 0){
	 //dpr($value_expert);
	}
	
	$d = array();
	foreach ($value_expert AS $array) {
		foreach ($array AS $eid => $value) {
			$d[$eid][] = $value;
		}
	}
	
	
	//dpr($d);
	
	
	
	
	$c = array();
	foreach ($d AS $eid => $arr) {
		$c[$eid] = round(array_sum($d[$eid])/$count,1);
	}
	
	//dpr($c);
	
	
	return $c;
	
	
	
	/*
	$query = "SELECT ROUND(SUM(value)/%d, 1) as `average_sum`, eid FROM {expas_data_values} WHERE nid = %d GROUP BY eid ORDER BY eid ASC";
	$result = db_query($query, $count, $zayavka_nid);
	$vals = array();
	while($row = db_fetch_object($result)) {
		$koef = expas_form_get_coef($row->eid);
		$vals[$row->eid] = $row->average_sum*$koef;
	}*/
	//return $vals;
}

/**
 * Собираем кнопки 
 * для скачивания форм в HTML or PDF
 *
 * @param str $value
 * @param str $callback
 * @param int $nid
 * @return unknown
 */
function expas_user_print_forms_button($nid, $type = 'html') {
	
	$title = ($type == 'html') ? 'Открыть форму'  : 'PDF формы';
	//$ready_print[$nid] = util_user_print_forms_ready($nid);
	$attr = array('class' => array('user-print-form'));
	//if(!$ready_print[$nid]) {
//		$attr = array('disabled' => 'disabled', 'title' => 'Форма для этой заявки еще не готова для скачивания.');
	//}
	$form[1] = array(
		'#type' => 'submit',
		'#value' => $title.' 2',
		'#submit' => array('util_user_print_form_2'),
		'#attributes' => $attr,
	);
	$form[2] = array(
		'#type' => 'submit',
		'#value' => $title.' 3',
		'#submit' => array('util_user_print_form_3'),
		'#attributes' => $attr,
	);
	$form[3] = array(
		'#type' => 'submit',
		'#value' => $title.' 5',
		'#submit' => array('util_user_print_form_5'),
		'#attributes' => $attr,
	);
	$form['org_nid'] = array(
		'#type' => 'hidden',
		'#value' => $nid,
	);
	$form['type_show_document'] = array(
		'#type' => 'hidden',
		'#value' => $type,
	);
	return $form;
}

function expas_dpr($data) {
	print '<textarea>';
	print_r($data);
	print '</textarea>';
}

/*узнаем статус работы эксперта - начал или окончил работу*/
function expas_expert_status($uid) {
	$data = db_query("SELECT ready FROM {expas_experts_work} WHERE uid=:uid", array(':uid' => $uid))->fetchfield();
	return $data;
}
/*скачивание сводного заключения по конкретной программе*/
function expas_current_experts_evaluations($form, &$form_state, $programma_nid) {
	$form['programma_nid'] = array(
		'#type' => 'hidden',
		'#value' => $programma_nid,
	);
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Сводное заключение',
	);
	return $form;
}