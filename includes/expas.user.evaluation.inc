<?php
/**
 * @file
 * закладка эксперта в личном кабинете
 */
function expas_users_evaluations_list($account) {
	global $user;
	
	$args = func_get_args();
	$expas_status = variable_get('expas_status', 0);
	if(user_access('expas moderator') && $user->uid == $account->uid) {//раздел для модератора
		$output = '';
		$expas_views_id = variable_get('expas_views_id', '');
		$expas_views_page = variable_get('expas_views_page', '');
		$f = drupal_get_form('expas_moderator');
		$output .= drupal_render($f);
		$output .= expas_moderator_list();
//		if((boolean)db_result(db_query("SELECT vid FROM {views_view} WHERE name = '%s'", $expas_views_id)) && (boolean)db_result(db_query("SELECT vid FROM {views_display} WHERE id = '%s'", $expas_views_page))) {
		//if((boolean)db_query("SELECT vid FROM {views_view} WHERE name=:name", array(':name' => $expas_views_id))->fetchField() && (boolean)db_query("SELECT vid FROM {views_display} WHERE id=:id", array(':id' => $expas_views_page))->fetchField()) {
		//	$views = expas_embed_view($expas_views_id, $expas_views_page, array());
		//	$output .= $views;
		//}
		if ($expas_views_id || $expas_views_page) {
			$output .= '<div class="error">Указанные в настройках Views параметры неточны или неполны.</div>';
		}
		return $output;
	}
	else if (user_access('expas access evaluation', $account)) {//раздел для эксперта 
		$expas_status = variable_get('expas_status', 0);
		if(!$expas_status) {
			return '<span class="warning">Экспертиза еще не началась.</span>';
		}
		$expert_status = expas_expert_status($account->uid);
		
		$query = "SELECT IF(expert_relation = '', 'expas_experts_relation_standart', expert_relation) as `function_relation` FROM {expas_type_node} WHERE enabled = 1";
		$result = db_query($query)->fetchAll();
		$nids = array();
		
		foreach($result AS $row) {
			if(function_exists($row->function_relation)) {
				$nids += call_user_func($row->function_relation, $args[0]);
			}
		}
		$header = array(
			array('data' => 'Наименование проекта', 'width' => '75%'),
			
			//array('data' => 'ВУЗ'),
			array('data' => 'Оценка эксперта'),
			//array('data' => 'Выгрузки'),
		);
		$rows = array();
		foreach($nids AS $ever_nid) {
			$node = node_load($ever_nid);
			$ball = expas_expert_ever_values($account->uid, $node->nid);
			if(!$expert_status) {
				$link_form = l($node->title, '/', array('attributes' => array('onClick' => 'alert("Вы должны начать работу.");return false;')));
			}			
			else {
				$link_form = l($node->title,'user_evaluation/'.$node->nid.'/'.$account->uid);
			}
			//$f = drupal_get_form('expas_user_print_forms_button', $node->nid, 'html');
			$rows[] = array(
				array('data' => $link_form),
				//array('data' => $vuz_name),
				array('data' => $ball),
				//array('data' => render($f) ) ,
			);
		}
		//кнопка готовности эксперта
		//debug($account->uid);
		$f = drupal_get_form('expas_status_expert_button', $account->uid);
		$output = render($f);
		if ($expert_status == 1 && expas_expert_show_button($account)) {
			//$output .= '<div style="color: #CA561B">Чтобы распечатать свои экспертные заключения вы должны завершить работу.</div>';
		}
		/*else if ($expert_status == 2 && expas_expert_show_button($account)) {
			$f = drupal_get_form('expas_form_button_print_evals', $account->uid);
		  $output .= render($f);
		}*/
    if(user_access('expas moderator') && user_access('expas access evaluation', $account)) {
      $f = drupal_get_form('expas_management_status_expert_form', $account->uid, $expert_status);
      $output .= render($f);
    }
		$output .= theme('table', array('header' => $header, 'rows' => $rows));
		return $output;
	}
}

function expas_moderator() {
	global $user;
	$status = variable_get('expas_status', 0);
	drupal_set_title($user->name.': Экспертиза <span '.(($status) ? 'style="color:green">включена' : 'style="color:red">выключена').'</span>', PASS_THROUGH);	
	$form['_block'] = array(
		'#type' => 'fieldset',
		'#title' => 'Настройки',
		'#collapsible' => 1,
		'#collapsed' =>1,
	);
	$form['_block']['excel'] = array(
		'#type' => 'checkbox',
		'#title' => 'Печать в Excel',
	);
	$form['_block']['print_html'] = array(
		'#type' => 'submit',
		'#value' => 'Распечатать все заявки в HTML',
		'#submit' => array('_expas_print_excel'),
		'#access' => true,
	);
	$form['_block']['block_'] = array(
		'#type' => 'fieldset',
		'#title' => 'Статус экспертизы',
	);
	$form['_block']['block_']['expas_status'] = array(
		'#type' => 'checkbox',
		'#default_value' => $status,
		'#title' => 'Включено',
		'#description' => 'Если выключено, эксперты не смогут больше редактировать свои оценки. А так же, проводить экспертизу.',
	);
	$form['_block']['block'] = array(
		'#type' => 'fieldset',
		'#title' => 'Отобразить Views',
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
		'#description' => 'На этой странице будет отображен любой views, который вы укажете.',
		'#suffix' => '<br />',
	);
	$form['_block']['block']['expas_views_id'] = array(
		'#title' => 'Id views',
		'#type' => 'textfield',
		'#attributes' => array('style' =>'width:200px'),
		'#default_value' => variable_get('expas_views_id', ''),
	);
	$form['_block']['block']['expas_views_page'] = array(
		'#title' => 'Page views',
		'#type' => 'textfield',
		'#attributes' => array('style' =>'width:200px'),
		'#default_value' => variable_get('expas_views_page', ''),
	);
	$form['_block']['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Сохранить',
		'#submit' => array('_expas_save_moderate'),
	);
	return $form;
}
/**
 * Возвращает массив идентификаторов узлов 
 * стандартная функция отношения эксперта к узлам
 * для отображения в кабинете эксперта
 *
 * @param int $user_id
 */
function expas_experts_relation_standart($user_id) {
	$query = "SELECT n.nid FROM {node} n INNER JOIN {expas_type_node} ent ON n.type = ent.type_node WHERE ent.enabled = 1 ORDER BY n.created DESC";
	$result = db_query($query);
	$nids = array();
//	while($nid = db_result($result)) {
	foreach ($result AS $nid ) {
		$nids[] = $nid->nid;
	}
	return $nids;
}
/**
 * callback function
 * скачать excel представление заявок с оценками
 */
function _expas_print_excel(&$form, &$form_state) {
	//$module_path = drupal_get_path('module','expas');
	//include_once($module_path.'/excel/Tests/requests.php');
	//sleep(2);
	//drupal_goto($GLOBALS['base_url'].'/'.drupal_get_path('module','expas').'/excel/Tests/requests.xls');
	$args = array();
  if($form_state['values']['excel']) {
	  header('Content-Type: text/html; charset=utf-8');
		header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0', FALSE);
		header('Pragma: no-cache');
		header('Content-transfer-encoding: binary');
		header('Content-Disposition: attachment; filename=vse-zayavki.xls');
		header('Content-Type: application/x-unknown');
  }
  print theme('util_all_zayavka_excel', $args);
	exit();
}
/**
 * callback function
 * сохраняем настройки вьюза для модератора
 */
function _expas_save_moderate(&$form, &$form_state) {
	variable_set('expas_views_id', $form_state['values']['expas_views_id']);
	variable_set('expas_views_page', $form_state['values']['expas_views_page']);
	variable_set('expas_status', $form_state['values']['expas_status']);
}
/*форма управлением готовности эксперта*/
function expas_management_status_expert_form() {
    $args = func_get_args();
    $form['expert_status_change'] = array(
        '#type' => 'select',
        '#options' => array('Работа не начата', 'Приступил к работе', 'Закончил работу'),
        '#default_value' => $args[2],
        '#prefix' => '<div class="container-inline">',
    );
    $form['uid'] = array(
        '#type' => 'hidden',
        '#value' => $args[1],
    );
    $form['submit'] = array(    
        '#type' => 'submit',
        '#value' => 'Поменять готовность эксперта',
        '#attributes' => array('onClick' => 'return confirm("Вы действительно желаете поменять статус готовновсти Эксперта?")'),
        '#suffix' => '</div>',
    );
    return $form;
}
//обратный вызов сохоанения готовности эксперта
function expas_management_status_expert_form_submit(&$form, &$form_state) {
    //$query = "UPDATE {util_experts_work} SET ready=:ready, date_update=UNIX_TIMESTAMP() WHERE uid=:uid";
    $ok = db_update('expas_experts_work')
    ->fields(array
    	(
    		'ready' => $form_state['values']['expert_status_change'],
    		'date_update' => time(),
    	)
    )
    ->condition('uid', $form_state['values']['uid'])
    ->execute();    
    if($ok) {
        drupal_set_message('Статус экперта был благополучно изменен.');
    }
    else {
        drupal_set_message('Ошибка при смене статуса экперта','error');
    }
}
/*вывод кнопок готовности для юзверя*/
function expas_status_expert_button($form, &$form_state, $uid) {
  $expert_status = expas_expert_status($uid);
	if(!$expert_status) {
		$expert_is_append = db_query("SELECT COUNT(*) FROM {field_data_" . FIELD_NODETYPE_EXPERTS . "} WHERE " . FIELD_NODETYPE_EXPERTS . "_value=:uid", array(':uid' => $uid))->fetchField();
		$form['start'] = array(
			'#type' => 'submit',
			'#value' => 'Начать работу',
			'#attributes' => array('onClick' => 'return confirm("Подтвердите, что вы хотите начать работу.")'),
			'#access' => ($expert_is_append > 0),
		);
		$form['start_'] = array(
			'#type' => 'item',
			'#value' => 'Вы являетесь экспертом, но Вы не прикреплены ни к одной программе конкурса.',
			'#access' => !$expert_is_append,
		);
	}
	else if ($expert_status == 1) {
		$count_no_end_exp = db_query("SELECT COUNT(*) FROM {field_data_" . FIELD_NODETYPE_EXPERTS . "} e 
																						LEFT JOIN {expas_data_values} v ON v.nid = e.entity_id 
																						WHERE e." . FIELD_NODETYPE_EXPERTS . "_value=:uid && v.nid IS NULL", array(':uid' => $uid))->fetchField();
		if ($count_no_end_exp == 0) {//все оценены
			$form['start2'] = array(
				'#type' => 'submit',
				'#value' => 'Завершить работу',
				'#attributes' => array('onClick' => 'return confirm("Подтвердите, что вы хотите завершить работу.")'),
			);
		}
		else {
			$form['start_'] = array(
				'#type' => 'item',
				'#value' => 'Вы начали работу. Неоцененных программ - <strong>'.$count_no_end_exp.'</strong>',
			);
		}
		
	}
	else {//2
		$form['end'] = array(
			'#type' => 'item',
			'#prefix' => '<strong>Вы завершили работу.</strong>',
		);
	}
	$form['uid'] = array(
		'#type' => 'hidden',
		'#value' => $uid,
	);
	return $form;
}
/*сохранялка нажатие готовности юзверя*/
function expas_status_expert_button_submit(&$form, &$form_state) {
	$value = null;
	$uid = $form_state['values']['uid'];
	if (isset($form_state['values']['start'])) {
		$value = 1;
	}
	else if (isset($form_state['values']['start2'])) {
		$value = 2;
	}
	if(db_query("SELECT uid FROM {expas_experts_work} WHERE uid=:uid", array(':uid' => $uid))->fetchField()) {
		//db_query("UPDATE {util_experts_work} SET ready = %d, date_update = UNIX_TIMESTAMP() WHERE uid = %d", $value, $uid);
		db_update("expas_experts_work")
		->fields(array("ready" => $value, "date_update" => time()))
		->condition("uid", $uid)
		->execute();
	}
	else {
//		db_query("INSERT INTO {util_experts_work} SET ready = %d, date_update = UNIX_TIMESTAMP(), uid = %d", $value, $uid);
		db_insert("expas_experts_work")
		->fields(array("ready" => $value, "date_update" => time(), "uid" => $uid))
		->execute();
	}
}
/**
вывод списка программ для модератора
*/
function expas_moderator_list() {
	$output = drupal_get_form('expas_filter_moderator_list');
	$filter_a = variable_get('expas_filter_a', -1);
	$filter_b = variable_get('expas_filter_b', -1);
	$add_sql = ""; $output = "";
	$select = db_select("node","n")->extend('TableSort');
	//$select->innerJoin("field_data_".FIELD_NODETYPE_EXPERTS, "exp", "exp.entity_id=n.uid");
	$select->fields("n", array("nid",  "title",  "created"))
	->extend('PagerDefault')
	->condition("type", "page")
	->condition('status', 1)
	->limit(20);
	if($filter_b > 0) {
		$select->where("n.nid IN(SELECT entity_id FROM {".FIELD_NODETYPE_EXPERTS."} WHERE ".FIELD_NODETYPE_EXPERTS."_value=:val)", array(":val" => $filter_b));
	}
	$header = array(
		array('data' => 'Проект', 'field' => 'title', 'sort' => 'asc'),
		array('data' => 'Эксперты'),
		array('data' => 'Ср.оценка'),
		array('data' => 'Создан','field' => 'created'),
		array('data' => 'Отчет'),
	);
	$select->orderByHeader($header);
	//debug($select);
	$results = $select->execute();
	//$query .= tablesort_sql($header);
	//$result = pager_query($query, 40, 0, NULL);
	foreach ($results AS $row) {
	  $ball = expas_average_ball($row->nid, false, false, true, true);
		$rows[] = array(
			array('data' => l($row->title, 'node/'.$row->nid), 'style' => 'width:25%'),
			array('data' => expas_list_experts($row->nid)),
			array('data' => $ball['average'], 'style' => 'text-align:center'),
			array('data' => format_date($row->created ,'custom','d M, Y H:i')),
			array('data' => drupal_get_form('expas_current_experts_evaluations', $row->nid)),
		);
	}
	
	$output .= theme('table', array('header' => $header, 'rows' => $rows));
	$output .= theme('pager');
	return $output;
}
/*выводим фильтр для модератора*/
function expas_filter_moderator_list() {
	$form['select'] = array(
		'#type' => 'select',
		'#title' => 'Готовность',
		'#options' => array(-1 => '- Все - ', 0 => 'Не завершенные', 1 => 'Завершенные'),
		'#default_value' => variable_get('expas_filter_a', -1),
		//'#attributes' => array('class' => 'container-inline'),
	);
	$experts = array(-1 => '-Все эксперты-') + expas_cck_get_experts(false, true, false);
	$form['experts'] = array(
		'#type' => 'select',
		'#title' => 'Эксперты',
		'#options' => $experts,
		'#default_value' => variable_get('expas_filter_b', -1),
	);
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Искать',
		//'#attributes' => array('class' => 'container-inline'),
	);
	$form['clear'] = array(
		'#type' => 'submit',
		'#value' => 'Очистить',
		'#submit' => array('util_filter_moderator_list_clear'),
	);
	return $form;
}
/**
 * Возвращает список експертов 
 * для построения cck-поля в программе
  @param  array $uids = массив из uid экспертов
  @param  int $count - узнаем кол-во программ, которые прикреплены к каждому экперту
 */
function expas_cck_get_experts($uids = false, $count = false, $html=true, $ready=true) {
  $add_sql = "";
  if($uids[0] != "") {
		$add_sql = "&& u.uid IN (".implode(",",$uids).")";
	}
	$ready_texts  = array(0 => 'не приступал', 1 => 'начал работу', 2 => 'закончил работу');
	$query = "SELECT n.uid 
	          FROM node n 
            INNER JOIN users u ON u.uid = n.uid && u.status = 1
            INNER JOIN field_data_field_user_family family ON family.entity_id = n.uid 
            INNER JOIN users_roles ur ON ur.uid = u.uid && ur.rid = 6
            WHERE n.uid NOT IN(0,1) ".$add_sql."
            ORDER BY family.field_user_family_value ASC";
	$result = db_query($query)->fetchAll();
	$data = array();
	$add_count = '';
	$prefix_html = ($html) ? '<small><em>' : '';
	$suffix_html = ($html) ? '</em></small>' : '';
	foreach ($result AS $row) {
		$user = user_load($row->uid);
		
	  $add_ready = ''; 
		if($ready) {
			$expert_status = util_expert_status($row->uid);
			$add_ready = '('.$ready_texts[intval($expert_status)].')';
		}
		if($count) {
			$add_count = ' '.$prefix_html.'(Прикреплен к '. intval(db_query("SELECT COUNT(*) FROM {field_data_".FIELD_NODETYPE_EXPERTS."} WHERE ".FIELD_NODETYPE_EXPERTS."_value=:uid", array(':uid' => $row->uid))->fetchField()).' пр.) '. $suffix_html;
		}
		$data[$row->uid] = _expas_get_fio($user) .  $add_count . ' ' . $add_ready;
	}
	return $data;
}
/*
  выводим экспертов в списке модератора
*/
function expas_list_experts($nid) {
	$node = node_load($nid);
	$array_experts = array();
	
	if(empty($node->{FIELD_NODETYPE_EXPERTS}['und'][0]['value'])) {
		return 'Нет';
	}
	else {
		foreach ($node->{FIELD_NODETYPE_EXPERTS}['und'] AS $arr) {
		  $array_experts[] = $arr['value'];
		}
	}
	$ready_texts  = array(0 => 'не приступал', 1 => 'начал работу', 2 => 'закончил работу');
	$experts = expas_cck_get_experts($array_experts, false,  false,  false);
	$output = '<ol>';
	if(is_array($experts)) {
		foreach ($experts AS $uid => $fio) {
		  $expert_status = expas_expert_status($uid);
		  $ball = expas_average_ball($nid, false, $uid, true, true);
			$output .= '<li>'.l($fio, 'user/'.$uid) . ' ('.l('Оценка:','user_evaluation/'.$nid.'/'.$uid).' <strong>'.floatval($ball['average']).'</strong>, прикреплен к <strong>'.db_query("SELECT COUNT(*) FROM {content_field_programma_experts} WHERE field_programma_experts_value=:uid", array(':uid' => $uid))->fetchfield().'</strong> пр., '.$ready_texts[intval($expert_status)].')</li>';
		}
	}
	$output .= '</ol>';
	return $output;
}