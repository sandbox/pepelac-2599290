<?php
// $Id$
// тема вывода блока вопросов для конкретного узла
print '<div id="expas_show_block" class="ajax_quest_width">';
print '<form id="block-234" method="POST" action="" accept-charset="UTF-8">';
//print '<div class="description move_right">Блоки вопросов</div>';
//$result = db_query("SELECT * FROM {expas_blocks} WHERE id_form_type_node = %d", $args[0]);
$result = db_query("SELECT * FROM {expas_blocks} WHERE id_form_type_node=:type", array(':type' => $args[0]))->fetchAll();
$count = 0;
//while($row = db_fetch_object($result)){
foreach ($result AS $row) {
	$form['a'.$count] = array(
		'#type' => 'markup',
		'#markup' => '<div class="expas_question" value="'.$row->category_id.'" id="question_'.$row->category_id.'">'.$row->title.'<div>вопросов: '.db_query('SELECT count(*) FROM {expas_questions} WHERE chapter=:chapter', array(':chapter' => $row->category_id))->fetchField().'</div>'."\r\n".'</div><input type="checkbox" name="expas_delete_block['.$row->category_id.']">'."\r\n",
		'#weight' => $row->category_id,
	);
	$count++;
}

//$form['b'] = array(
//	'#type' => 'markup',
//	//'#markup' => '<div class="container-inline">',
//	'#weight' => 999,
//);

$form['submit'] = array(
	'#type' => 'submit',
	'#value' => ' -- ',
	'#attributes' => array('onClick' => 'return confirm("Удалить отмеченные блоки?");','class' => array('delete')),
	'#access' => $count > 0,
	'#weight' => 1000,
);

//$form['c'] = array(
//	'#type' => 'button',
//	'#value' => ' + ',
//	'#attributes' => array('onClick' => 'expas_hide_show(\'block-add-234\');return false;','class' => array('add')),
//	'#weight' => 1001,
//	//'#suffix' => '</div>',
//);

$form['add_block'] = array(
	//'#prefix' => '<div id="block-add-234">',
	'#type' => 'fieldset',
	'#attributes' => array('style' => 'width:190px'),
	'#weight' => 1002,
	'#title' => 'Новый блок',
	'#access' => 1,
);
$form['add_block']['expas_title_block'] = array(
	'#type' => 'textarea',
	'#value' => '',
	'#attributes' => array('style' => 'width:185px'),
	'#weight' => 1,
);
$form['add_block']['submit1'] = array(
	'#type' => 'submit',
	'#attributes' => array('onClick' => 'return confirm("Создать новый блок?");', 'class' => array('add')),
	'#value' => 'Создать',
	'#weight' => 2,
	//'#suffix' => '</div>',
);
$form['#submit'][] = 'expas_add_block';
$form['nodetype'] = array(
	'#type' => 'hidden',
	'#value' => $args[0],
);
$form['block'] = array(
	'#type' => 'hidden',
	'#value' => 'false',
);
$form['question'] = array(
	'#type' => 'hidden',
	'#value' => 'false',
);


print '<TABLE  border="0" cellpadding="4" cellspacing="4" width="600px" class="expas-border-hidden">' . "\r\n" . '<tr>' . "\r\n" . '<td style="100px;" class="top">'."\r\n";
$form_state = array();
//$form = form_builder('block-234' , $form, $form_state);
print drupal_render($form);
print '</td>'."\r\n".'<td style="width:500px" class="top" id="expas_data_question"></td>'."\r\n".'</tr>'."\r\n".'</TABLE>'."\r\n";
print '</form>'."\r\n";
print '</div>'."\r\n";

