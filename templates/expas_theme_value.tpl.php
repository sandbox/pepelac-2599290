<?php
// $Id$
// тема вывода значений для вопроса 
print '<div id="expas_show_value" class="solid_border">';
print '<form id="sd1232" method="post" action="" accept-charset="UTF-8">';
$result = db_query("SELECT * FROM {expas_values} WHERE eid = :eid ORDER BY value ASC", array(':eid' => $args[0]))->fetchAll();
$count = 0;
//while($row = db_fetch_object($result)) {
foreach($result AS $row) {
	$form['val'.$row->id] = array(
		'#type' => 'fieldset',
		'#attributes' => array('style' => 'border: 1px solid gray'),
		'#description' => $row->description,
	);
	$form['val'.$row->id]['value_delete['.$args[0].']['.$row->id.']'] = array(
	 '#title' => (string)$row->value.'.',
	 '#type' => 'checkbox',
	 
	);
	$count++;
}
$form['delete'] = array(
	'#type' => 'submit',
	'#prefix' => '',
	'#value' => ' -- ',
	'#attributes' => array('onClick' => 'return confirm("Удалить отмеченные значения?")', 'class' => 'delete'),
	'#access' => $count > 0,
	'#suffix' => '<p />',
);
$form['new_value'] = array(
	'#type' => 'fieldset',
	'#prefix' => '<p />',
	'#attributes' => array('style' => 'border: 1px solid gray'),
	'#title' => 'Новое значение',
);
$form['new_value']['value_add['.$args[0].']'] = array(
	'#type' => 'textfield',
	'#title' => 'значение',
	'#weight' => 0,
	'#id' => 'select-value-100',
	'#required' => TRUE,
	'#size' => 4,
);
$form['new_value']['value_add_descr['.$args[0].']'] = array(
	'#type' => 'textarea',
	'#title' => 'описание',
	'#weight' => 1,
	'#maxlength' => 1000,
	'#id' => 'select-value-100',
	'#required' => TRUE,
	'#size' => 2,
	'#attributes' => array('style' => 'width:150px')
);
$form['new_value']['submit'] = array(
	'#type' => 'submit',
	'#weight' => 2,
	'#value' => 'внести',
	'#attributes' => array('onClick' => 'return confirm("Добавить новое значение?")','class' => 'add'),
);
$form['question'] = array(
	'#type' => 'hidden',
	'#value' => $args[0],
);
$block  = db_result(db_query('SELECT chapter FROM {expas_questions} WHERE eid = %d LIMIT 1', $args[0]));
$form['block'] = array(
	'#type' => 'hidden',
	'#value' => $block,
);
$nodetype = db_result(db_query('SELECT id_form_type_node FROM {expas_blocks} WHERE category_id = %d', $block));
$form['nodetype'] = array(
	'#type' => 'hidden',
	'#value' => $nodetype,
);


print '<TABLE border="0" cellpadding="4" cellspacing="4" style="width:200px" class="expas-border-hidden">' . "\r\n" . '<tr>' . "\r\n" . '<td style="width:300px">';
$form_state = array();
$form = form_builder('sd1232' , $form, $form_state);
print drupal_render($form);
print '</td></tr>'."\r\n".'</TABLE>'."\r\n";
print '</form>';
print "\r\n".'</div>'."\r\n";

