<?php
// $Id$
// тема отображения блока сбоку списка оценивших ресурс на узле
$node = menu_get_object();
$search_id = explode("/", $GLOBALS['_GET']['q']);
$uid = (int)$search_id[3];
if(!expas_is_resourse_evaluation($node)) {
	return false; 
}
$data_eval = expas_average_ball($node->nid);	
print '<div class="block_average_ball">'.$data_eval['average'].'</div><p />';	
//$result = db_query("SELECT DISTINCT(uid) FROM {expas_data_values} WHERE nid = %d", $node->nid);
$result = db_query("SELECT DISTINCT(uid) FROM {expas_data_values} WHERE nid = :nid", array(':nid' => $node->nid))->fetchAll();
$is_util = (function_exists('_util_get_fio')) ? true : false;
//while($row = db_fetch_object($result)) {
foreach ($result AS $row) {
	if($is_util) {
		$fio = _util_get_fio(user_load($row->uid));
	}
	else {
//		$fio = db_result(db_query("SELECT name FROM {users} WHERE uid = %d", $row->uid));
		$fio = db_query("SELECT name FROM {users} WHERE uid = :uid", array(':uid' => $row->uid))->fetchField();
	}
	
	$class = ($uid == $row->uid) ? 'expas_select_eval_user' : '';
	print '<div class="'.$class.'">'.l('Оценил: '.$fio, $GLOBALS['base_url'].'/node/'.$node->nid.'/expas_evaluations/'.$row->uid).'</div>';
}
 