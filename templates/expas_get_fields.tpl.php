<?php
// $Id$
// тема вывода текстовых оценочных полей
//	$result = db_query("SELECT * FROM {expas_fields}");
	$result = db_query("SELECT * FROM {expas_fields}")->fetchAll();
	$form = array();
	$form['qqw'] = array(
		'#type' => 'markup',
		'#prefix' => '<div id="expas-block-fields">',
	);
	$count = 0;
	foreach ( $result AS $row ) {
		$form['fields['.$row->fid.']'] = array(
			'#prefix' => '<div class="container-inline">',
			'#type' => 'textfield',
			'#size' => 100,
			'#default_value' => $row->field,
			'#weight' => $row->fid,
		);
		
		$form['update_fields['.$row->fid.']'] = array(
			'#type' => 'checkbox',
			'#title' => 'обновить',
			'#weight' => $row->fid + 0.1,
		);
		
		$form['delete_fields['.$row->fid.']'] = array(
			'#type' => 'checkbox',
			'#title' => 'удалить',
			'#weight' => $row->fid + 0.2,
			'#suffix' => '</div>',
		);
		$count++;
	}
	
	$form['q'] = array(
		'#type' => 'button',
		'#ajax' => array(
			'callback' => 'expas_fields/update_delete',
			'wrapper' => 'expas-block-fields',
			'method' => 'replace',
			'effect' => 'fade',
		),
		'#value' => 'Выполнить операцию с выделенными полями',
		'#attributes' => array('class' => array('add'), 'title' => 'Выполнить действие','return false;'),
		'#weight' => 10000,
		'#suffix' => '<br /><br />',
		'#prefix' => '<br />',
		'#access' => $count > 0,
	);
	
	$form['field_add'] = array(
			'#prefix' => '<div class="container-inline">',
			'#type' => 'textfield',
			'#size' => 100,
			'#weight' => 10001,
	);
	
	$form['sd'] = array(
		'#type' => 'button',
		'#value' => ' + ',
		'#attributes' => array('class' => array('add')),
		'#ajax' => array(
			'callback' => 'expas_field_add',
			'wrapper' => 'expas-block-fields',
			'method' => 'replace',
			'effect' => 'fade',
		),
		'#suffix' => '</div>',
		'#weight' => 10002,
	);
	
	$form['a'] = array(
		'#type' => 'markup',
		'#markup' => '</div>',
		'#weight' => 10003,
	);
//	$form_state = array();
//	$form = form_builder('form-2420942098420',  $form, $form_state);
	print drupal_render($form);