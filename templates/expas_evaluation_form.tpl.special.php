<?php
// $Id$
// тема для отображения оценочной формы в узле
print '<style>legend{font-size:14px;}</style>';
global $user;
$expert_uid = (!$expert) ? $user->uid : $expert;
if(function_exists('_util_get_fio')) {
	$fio = _util_get_fio(user_load($expert_uid));
}


$count = 0;
$form['block_questions'] = array(
	'#prefix' => '<form method="POST" id="eval-form-'.$node->nid.'-'.$expert_uid.'" action="" accept-charset="UTF-8">',
	'#type' => 'markup',
	//'#collapsible' => TRUE,
	//'#collapsed' => (!$GLOBALS['_POST']) ? TRUE : FALSE,
	'#title' => 'Экспертная оценка материала'.' '.$fio,
	'#suffix' => '</form>',
);

//Блоки вопросов
//$query_block = db_query("SELECT * FROM {expas_blocks} WHERE id_form_type_node IN (SELECT cid FROM {expas_type_node} WHERE type_node = '%s' && enabled = 1) ORDER BY category_id ASC", $node->type);
$query_block = db_query("SELECT * FROM {expas_blocks} WHERE id_form_type_node IN (SELECT cid FROM {expas_type_node} WHERE type_node = :type && enabled = 1) ORDER BY category_id ASC", array(':type' => $node->type))->fetchAll();
//while($row_b = db_fetch_object($query_block)) {
foreach ($query_block AS $row_b) {
	$form['block_questions']['block'.$row_b->category_id] = array(
		'#type' => 'fieldset',
		'#title' => $row_b->title,
		'#weight' => $count,
			
	);
	if(!$type_node_id)
		$type_node_id = $row_b->id_form_type_node;
	// список вопросов
//	$query_question = db_query("SELECT * FROM {expas_questions} WHERE chapter IN (SELECT category_id FROM {expas_blocks} WHERE id_form_type_node = %d) && chapter = %d", $row_b->id_form_type_node, $row_b->category_id);
	$query_question = db_query("SELECT * FROM {expas_questions} WHERE chapter IN (SELECT category_id FROM {expas_blocks} WHERE id_form_type_node = :type) && chapter = :chapter", array(':type' => $row_b->id_form_type_node, ':chapter' => $row_b->category_id))
	->fetchAll();
//	while($row_q = db_fetch_object($query_question)) {
	foreach($query_question AS $row_q) {
		$expas_get_select_values = expas_get_select_values($row_q->eid);
		$set_value = -1;
		$special_description = null;
		if($row_q->eid == 3) {
			$finance = util_get_event_finance($node->nid);
			//dpm(util_get_value_formula_1($finance['ratio']) *  util_form_get_coef($row_q->eid) ) ;
			$set_value = round(util_get_value_formula_1($finance['ratio']) /* * util_form_get_coef($row_q->eid)*/);
			//$set_value = 1;
			$special_description = '<span style="color:green">Заполняется автоматически по формуле.</span>';
		}
		elseif ($row_q->eid == 6) {
			$set_value = round(util_get_value_formula_2($node->nid) /* * util_form_get_coef($row_q->eid)*/);
			$special_description = '<span style="color:green">Заполняется автоматически по формуле.</span>';
		}
		
		if($set_value > -1){
			$expas_get_select_values['options'] = array($set_value=>$set_value);
		}
		
		$form['block_questions']['block'.$row_b->category_id]['question['.$row_q->eid.']'] = array(
			'#type'  => 'select',
			'#required' => TRUE,
			'#options' => $expas_get_select_values['options'],
//			'#default_value' => (isset($_POST['question'][$row_q->eid])) ? $_POST['question'][$row_q->eid] : db_result(db_query("SELECT value FROM {expas_data_values} WHERE eid = %d && uid = %d && nid = %d", $row_q->eid, $expert_uid, $node->nid)),
			'#default_value' => (isset($_POST['question'][$row_q->eid])) ? $_POST['question'][$row_q->eid] : db_query("SELECT value FROM {expas_data_values} WHERE eid = :eid && uid = :uid && nid = :nid", array(':eid' => $row_q->eid, ':uid' => $expert_uid, ':nid' => $node->nid))->fetchField(),
			'#title' => $row_q->title . ' <span class="description">'.$row_q->description.'</span>',
			'#description' => ($special_description != null) ? $special_description : 'Заполнить '.expas_get_question_scale($row_q->eid),
			'#suffix' => '<div style="padding-top:-3px;"><span class="expas_descr">' . $expas_get_select_values['description'].'</span>',
		);
		$count++;		
	}
	$count++;
}
$result = db_query("SELECT * FROM {expas_fields}");
while($row = db_fetch_object($result)) {
	$form['block_questions']['eval_field['.$row->fid.']'] = array(
		'#type' => 'textarea',
		'#title' => $row->field,
//		'#default_value' => (isset($_POST['eval_field'][$row->fid])) ? $_POST['eval_field'][$row->fid] : db_result(db_query("SELECT value FROM {expas_fields_values} WHERE nid = %d && uid = %d && fid = %d", $node->nid, $expert_uid, $row->fid)),
		'#default_value' => (isset($_POST['eval_field'][$row->fid])) ? $_POST['eval_field'][$row->fid] : db_query("SELECT value FROM {expas_fields_values} WHERE nid = :nid && uid = :uid && fid = :fid", array(':nid' => $node->nid, ':uid' => $expert_uid, ':fid' => $row->fid))->fetchField(),
		'#weight' => $count++,
	);
}

$form['block_questions']['block_2'] = array(
	'#type' => 'fieldset',
	'#weight' => 100,
);


$form['block_questions']['block_2'][] = array(
	'#type' => 'submit',
	'#name' => 'save_eval',
	'#prefix' => '<div class="container-inline">',
	'#value' => 'Оценить ресурс',
	
//	'#ahah' => array(
//		'path' => 'eval_resourse',
//		'wrapper' => 'wrapper-block-evaluation',
//		'method' => 'replace',
//		'effect' => 'fade',
//	),
);
expas_evaluation_resourse();

$alert = '';
if(!$GLOBALS['_POST']['error'] && $GLOBALS['_POST']['nid']) {
	$alert = '<span style="color:green">Форма была сохранена.</span>';
	
}
elseif ($GLOBALS['_POST']['error'] == 1 && $GLOBALS['_POST']['nid']) {
	$alert = '<span style="color:red">Ошибка. Заполните все поля.</span>';
}

$form['block_questions']['block_2'][] = array(
	'#prefix' => '&nbsp;',
	'#type' => 'markup',
	'#markup' => $alert,
	'#attributes' => array('style' => 'color:green'),
	'#suffix' => '</div>',
	'#access' => sizeof($_POST) > 0,
);
$form['block_questions']['uid'] = array(
	'#type' => 'hidden',
	'#value' => $expert_uid,
);
$form['block_questions']['nid'] = array(
	'#type' => 'hidden',
	'#value' => $node->nid,
);

$form_state = array();
$form = form_builder('eval-form-'.$node->nid.'-'.$expert_uid, $form, $form_state);
print drupal_render($form);
