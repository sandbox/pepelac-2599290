<?php
// $Id$
// тема вывода вопросов для конкретного блока
print '<div id="expas_show_quest">';
print '<form id="questions-234" method="post" action="" accept-charset="UTF-8">';
//print '<div class="description">Вопросы выбранного блока</div>';
$result = db_query("SELECT * FROM {expas_questions} WHERE chapter = %d", $args[0]);
$count = 0;
while($row = db_fetch_object($result)){
	$form['question_'.$row->eid] = array(
		'#type' => 'markup',
		'#markup' => '<div class="expas_value" value="'.$row->eid.'" id="value_'.$row->eid.'">'.$row->title.'<div class="description">'.$row->description.' </div><div>значений: '.db_result(db_query('SELECT COUNT(*) FROM {expas_values} WHERE eid = %d', $row->eid)).'</div></div>'."\r\n".
		'<input type="checkbox" name="expas_delete_question['.$row->eid.']">'
		."\r\n".'',
		'#weight' => $row->eid,
	);
	$count++;
}

if(!$count) {
	$form[] = array(
		'#type' => 'markup',
		'#markup' => '<div class="description">Вопросов нет</div>',
	);
}

$form[] = array(
	'#type' => 'markup',
	'#markup' => '<div class="container-inline">',
	'#weight' => 999,
);

$form['delete_submit'] = array(
	'#type' => 'submit',
	'#value' => ' -- ',
	'#access' => $count > 0,
	'#attributes' => array('onClick' => 'return confirm("Удалить отмеченные вопросы?");','class' => 'delete'),
	'#weight' => 1000,
);
$form[] = array(
	'#type' => 'button',
	'#value' => ' + ',
	'#attributes' => array('onClick' => 'expas_hide_show(\'add-question-block\');return false;','class' => 'add'),
	'#weight' => 1001,
);
$form['block_new_question'] = array(
	'#prefix' => '<div id="add-question-block">',
	'#type' => 'fieldset',
	'#attributes' => array('style' => 'width:235px'),
	'#weight' => 1002,
	'#title' => 'Новый вопрос',
);
$form['block_new_question']['expas_title_question'] = array(
	'#type' => 'textarea',
	'#maxlength' => 1000,
	'#attributes' => array('style' => 'width:230px'),
	'#title' => 'Заголовок',
	'#required' => true,
	'#weight' => 0,
);
$form['block_new_question']['expas_descr_question'] = array(
	'#type' => 'textarea',
	'#title' => 'Описание',
	'#maxlength' => 1000,
	'#attributes' => array('style' => 'width:230px'),
	'#weigth' => 1,
);

$form['question'] = array(
	'#type' => 'hidden',
	'#value' => 'false',
);
$form['block'] = array(
	'#type' => 'hidden',
	'#value' => $args[0],
);
$nodetype = db_result(db_query('SELECT id_form_type_node FROM {expas_blocks} WHERE category_id = %d', $args[0]));
$form['nodetype'] = array(
	'#type' => 'hidden',
	'#value' => $nodetype,
);
$form['block_new_question']['submit'] = array(
	'#type' => 'submit',
	'#attributes' => array('onClick' => 'return confirm("Создать новый вопрос?");','class' => 'add'),
	'#value' => 'создать',
	'#weight' => 2,
	'#suffix' => '</div>',
);
print '<TABLE border="0" cellpadding="2" cellspacing="2" class="expas-border-hidden">' . "\r\n" . '<tr>' . "\r\n" . '<td class="top">';
$form_state = array();
$form = form_builder('questions-234' , $form, $form_state);
print drupal_render($form);
print '</td><td class="top" id="expas_data_value"></td>'."\r\n".'</tr>'."\r\n".'</TABLE>';
print '</form>'."\r\n".'</div>';
