<?php
// $id$
// тема оценок конкретного пользователя для общего просмотра
$count = 0;

//получаем ФИО
$user_eval = user_load($uid);

if(function_exists("_util_get_fio") && $user_eval->uid > 0) {
	$fio = _util_get_fio($user_eval->uid);
}
elseif($user_eval->uid > 0) {
	$fio = $user_eval->name;
}
else {
	$fio = "Пользователь неизвестен";
}

$form['block_questions'] = array(
	'#type' => 'fieldset',
	'#collapsible' => TRUE,
	'#collapsed' => (!$expand) ? TRUE : FALSE,
	//'#title' => 'Экспертная оценка: '.theme('username', db_fetch_object(db_query("SELECT * FROM {users} WHERE uid = %d", $uid))),
	'#title' => 'Экспертная оценка: '.$fio,
);

if(@$print) {
	$form['block_questions'] = array();
}


//Блоки вопросов
//$query_block = db_query("SELECT * FROM {expas_blocks} WHERE category_id IN (SELECT chapter FROM {expas_questions} WHERE eid IN (SELECT eid FROM {expas_values} WHERE eid IN (SELECT eid FROM {expas_data_values} WHERE uid = %d))) ORDER BY category_id ASC", $user_eval->uid);
$query_block = db_query("SELECT * FROM {expas_blocks} WHERE category_id IN (SELECT chapter FROM {expas_questions} WHERE eid IN (SELECT eid FROM {expas_values} WHERE eid IN (SELECT eid FROM {expas_data_values} WHERE uid = :uid))) ORDER BY category_id ASC", array(':uid' => $user_eval->uid))->fetchAll();
/*
$form['block_questions'][] = array(
	'#type' => 'markup',
	'#markup' => (!$print) ? theme('image', $user_eval->picture, false, false) : '',
	'#weight' => $count,
);*/

//$count_blocks = db_result(db_query("SELECT COUNT(*) FROM {expas_blocks} WHERE category_id IN (SELECT chapter FROM {expas_questions} WHERE eid IN (SELECT eid FROM {expas_values} WHERE eid IN (SELECT eid FROM {expas_data_values} WHERE uid = %d)))", $user_eval));

//return '';
$count_blocks = db_query("SELECT COUNT(*) FROM {expas_blocks} WHERE category_id IN (SELECT chapter FROM {expas_questions} WHERE eid IN (SELECT eid FROM {expas_values} WHERE eid IN (SELECT eid FROM {expas_data_values} WHERE uid=:uid)))", array(':uid' => $user_eval->uid))->fetchField();

if(@$print) {
	$table = '&nbsp;<TABLE border="1" cellpadding="4" cellspacing="0" bordercolor="#ccc" width="100%" class="expas_table">';
}
else {
	$table = '<TABLE id="expas_evaluation_user" border="0" cellpadding="4" cellspacing="4" width="100%" class="expas_table">';
}

foreach ($query_block AS $row_b) {
  $block_sum_eval = '';
  if(!@$type_node_id) {
		$type_node_id = $row_b->id_form_type_node;
	}
  if($count_blocks > 1) {
    $block_sum_eval = '<tr><td colspan="3" class="expas-text-right expas_show_block_end"><strong>'.(int)expas_get_user_evaluation($uid, $node->nid, $row_b->category_id).'</strong> из '.expas_get_max_summ_value($type_node_id, array($row_b->category_id)).' возможных</td></tr>'."\r\n";
  }
  $form['block_questions']['block'.$row_b->category_id] = array(
		'#prefix' => $table . '<tr class="expas_show_block"><td>'.$row_b->title.'</td><td>Шкала</td><td>Оценка</td></tr>',
		'#suffix' => $block_sum_eval . '</TABLE>',
		'#type' => 'markup',
		'#weight' => ++$count,
	);
	// список вопросов
//	$query_question = db_query("SELECT * FROM {expas_questions} WHERE chapter IN (SELECT category_id FROM {expas_blocks} WHERE id_form_type_node = %d) && chapter = %d", $row_b->id_form_type_node, $row_b->category_id);
	$query_question = db_query("SELECT * FROM {expas_questions} WHERE chapter IN (SELECT category_id FROM {expas_blocks} WHERE id_form_type_node = :type) && chapter = :chapter", array(':type' => $row_b->id_form_type_node, ':chapter' => $row_b->category_id))->fetchAll();
//	while($row_q = db_fetch_object($query_question)) {
	foreach($query_question as $row_q) {
		$expas_get_select_values = expas_get_select_values($row_q->eid);
		$form['block_questions']['block'.$row_b->category_id]['question['.$row_q->eid.']'] = array(
//			'#prefix' => '<tr><td class="expas_show_question" width="80%">'.$row_q->title.'</td><td width="15%">'.expas_get_question_scale($row_q->eid).'</td><td class="expas_show_value" width="5%">'.db_result(db_query("SELECT value FROM {expas_data_values} WHERE eid = %d && uid = %d && nid = %d", $row_q->eid, $uid, $node->nid)).'</td></tr>',
			'#prefix' => '<tr><td class="expas_show_question" width="80%">'.$row_q->title.'</td><td width="15%">'.expas_get_question_scale($row_q->eid).'</td><td class="expas_show_value" width="5%">'.db_query("SELECT value FROM {expas_data_values} WHERE eid = :eid && uid = :uid && nid = :nid", array(':eid' => $row_q->eid, ':uid' => $uid, ':nid' => $node->nid))->fetchField().'</td></tr>',
			'#type'  => 'markup',
		);
		$count++;		
	}
	$count++; 
}

//$result = db_query("SELECT * FROM {expas_fields}");
$result = db_query("SELECT * FROM {expas_fields}")->fetchAll();
//while($row = db_fetch_object($result)) {
foreach($result AS $row) {
//	$value = db_result(db_query("SELECT value FROM {expas_fields_values} WHERE nid = %d && uid = %d && fid = %d", $node->nid, $uid, $row->fid));
	$value = db_query("SELECT value FROM {expas_fields_values} WHERE nid = :nid && uid = :uid && fid = :fid", array(':nid' => $node->nid, ':uid' => $uid, ':fid' => $row->fid))->fetchField();
	$form['block_questions']['eval_field['.$row->fid.']'] = array(
		'#type' => 'markup',
		'#prefix' => ((@$print) ? '<br />' : ''),
		'#access' => $value,
		'#title' => $row->field,
		'#markup' => $value,
		'#weight' => ++$count,
	);
}


$form['block_questions'][] = array(
	'#prefix' => '&nbsp;',
	'#type' => 'markup',
	'#markup' => '<div class="expas-text-right expas-full-ball">всего: '.(int)expas_get_user_evaluation($uid, $node->nid).' из '.expas_get_max_summ_value($type_node_id).' возможных</div>',
	'#weight' => 100,
);

$form_state = array();
$form['#parents'] = true;
$form['#tree'] = false;
$form['#array_parents'] = false;
$form_state['complete form'] = $form;
@$form = form_builder('eval-form-'.$node->nid.'-'.$user->uid, $form, $form_state);
print drupal_render($form);
